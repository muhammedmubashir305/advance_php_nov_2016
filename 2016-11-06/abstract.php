<?php

//abstract
//design pattern
//class , method
// abstract class parentClass
// {
// 	function method1()
// 	{
// 		echo "method1 from class test1";
// 	}	

// 	abstract function method2();
// }


// class childClass extends parentClass
// {
// 	function method2()
// 	{
// 		echo "method2 in childclass";
// 	}
// }

// $obj = new childClass;
// $obj->method2();
// echo "<br>";
// $obj->method1();


abstract class vehicle
{
	abstract function start();
	abstract function run();
	abstract function stop();
}
?>