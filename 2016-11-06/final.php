<?php
//final
//variable
//class
//function
class test1
{
	final function method1()
	{
		echo "method1 from class test1";
	}
}

class test2 extends test1
{
	function method1()
	{
		echo "method1 from class test2";
	}
	function method2()
	{
		echo "method2 from class test2";
	}
}

$test2_obj = new test2();
$test2_obj->method1();
echo "<br>";
$test2_obj->method2();
?>