<?php
//PHP method overloading
//
class test
{
	//variable
	//functions
	//properties -> public, protected, private
	private $var1;
	private $var2;
	public function _set_var1($value)
	{
		$this->var1 = $value;
	}

	public function _get_var1()
	{
		return $this->var1;
	}

	function test_overloading()
	{
		$args = func_get_args();
		echo "<pre>";
		print_r($args);
		echo "</pre>";
	}

}

$obj = new test();
$obj->test_overloading(10,20);
// $obj->_set_var1(10);
// echo $obj->_get_var1();


?>