<?php
//PHP5.3+
//traits
//Laravel 5.2+
//php5.6

trait test1
{
	private $var1 = 10;
	
	function method1()
	{
		echo "method1 from trait test1";
	}
	function method2()
	{
		echo "method2 from trait test1";
	}

	function fooBar()
	{
		echo "function FooBar from trait test1";
	}
}

trait test2 
{
	function method3()
	{
		echo "method3 from trait test2";
	}
	function method4()
	{
		echo "method4 from trait test2";
	}	
}

trait combined 
{
	use test1,test2;	
}
//insteadof -> traits
//HOME WORK

class ekOrClass 
{
	use test1;

	function _get_var1()
	{
		return $this->var1;
	}	
	function _set_var1($value)
	{
		$this->var1 = $value;
	}

	function fooBar()
	{
		echo "function FooBar from ekOrClass";
	}

}

class something extends ekOrClass
{
	use test1;
	function fooBar()
	{
		echo "function FooBar from class something";
	}
}

// $obj = new ekOrClass;
// $obj->_set_var1(20);
// echo $obj->_get_var1();

// echo "<br>";
$obj_something = new something;
echo $obj_something->_get_var1();
echo $obj_something->fooBar();

?>