<?php
//interface, traits, closure
//SPL = Standard PHP library

//sorting
//encoding
//iterations
//serialization


interface test1
{
	function method1();
	function method2();
}
interface test2
{
	function method3();
	function method4();
}

class faizan
{
	function method1()
	{
		echo "method1 from class faizan";
	}
	function method2()
	{
		echo "method2 from class faizan";
	}
}

class demo extends faizan implements test1,test2
{

	
	function method3()
	{
		echo "method3 from demo class";
	}

	function method4()
	{
		echo "method4 from demo class";

	}
}

$demo_obj = new demo;
$demo_obj->method1();

?>