<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')
              ->references('id')->on('posts')
              ->onDelete('cascade');
            $table->string('comments');
            $table->boolean('published')->default(false);
            $table->timestamps();
        });

        /*
        CREATE TABLE `comments` (                                                                                    
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,                                                             
            `post_id` int(10) unsigned NOT NULL,                                                                       
            `comments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,                                                  
            `published` tinyint(1) NOT NULL DEFAULT '0',                                                               
            `created_at` timestamp NULL DEFAULT NULL,                                                                  
            `updated_at` timestamp NULL DEFAULT NULL,                                                                  
            PRIMARY KEY (`id`),                                                                                        
            KEY `comments_post_id_foreign` (`post_id`),                                                                
            CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE  
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci     
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
