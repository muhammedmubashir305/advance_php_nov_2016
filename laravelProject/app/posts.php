<?php

namespace App;
use App\comments;
use Illuminate\Database\Eloquent\Model;

class posts extends Model
{
    // protected $table = "post";
    // protected $primary = "id";
    protected $fillable = ["name","details"];

    public function comments()
    {
        return $this->hasMany('App\Comments',"post_id");
    }
}
