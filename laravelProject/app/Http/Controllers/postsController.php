<?php

namespace App\Http\Controllers;
use App\posts;
use Illuminate\Http\Request;
class postsController extends Controller
{
    function index()
    {
        $posts = posts::all();
        //return $posts;
        return view("posts.lists",['posts'=>$posts]);
    }

    function add()
    {
    	return view("posts.add");
    }

    function save(Request $request)
    {
    	//method#1
    	$data = $request->all();
    	$post = new posts;
    	$post->name = $data['name'];
    	$post->details = $data['details'];
    	$post->save();
		
		//method#2
    	// $data = $request->all();
    	// posts::create($data);

    	#method#3
    	//$post->save($request->all());	
    	return redirect("/post");
    }

    
    function edit(posts $post)
    {
    	// return $post;
    	return view("posts.edit",["post"=>$post]);
    }
    

    function edit($post_id)
    {
        $post = posts::find($post_id);
        return $post;
        return view("posts.edit",["post"=>$post]);
    }

    function update(Request $request, posts $post)
    {

    }



}
