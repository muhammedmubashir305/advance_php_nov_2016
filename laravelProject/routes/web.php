<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//laravel 5.3
//laravel 5.2

Route::get('/', function () {
    return view('welcome');
});

// Route::get("about", function(){
// 	$arr = ["<p style='color:red;'>hello</p>", "MySQL", "Laravel"]; //php5.5
// 	//return view("about")->with("arr",$arr); //resource/view/about.blade.php
// 	return view("pages.about", compact("arr")); //resource/view/about.blade.php
// });

// Route::get("services",function(){
// 	return view("pages.services");
// });

// middleware
// 
Route::get('/post', "postsController@index");

Route::get('/post/add',"postsController@add");
Route::post('/post/save',"postsController@save");


//for edit
Route::get('/post/{post}/edit',"postsController@edit");
Route::post('/post/{id}/edit',"postsController@update");


Route::get('comments/{post}',"commentsController@show");
Route::post('comments/{post}/save',"commentsController@save");

// Route::get();
// Route::get('/', function(){
// 	return view();
// });


// controller -> resource
// rou

