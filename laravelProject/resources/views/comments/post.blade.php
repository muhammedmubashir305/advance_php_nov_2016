@extends("layout")
@section("content")

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<h2>Post Details</h2>
	</div>

	<div class="col-md-6 col-md-offset-3">
		<h3>{{ $post->name }}</h3>
		<p>{{ $post->details }}</p>
	</div>
</div>


<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<ul class="list-group">
		@foreach($comments as $comment)
			<li class="list-group-item">
				{{ $comment->comments }}
			</li>
		@endforeach
		</ul>
	</div>
</div>


<!-- http://laravel.dev/comments/2/save -->
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		{!! Form::open(['action'=>['commentsController@save',$post->id]]) !!}
			<div class="row">
				<textarea class="form-control" name="comments"></textarea>
			</div>
			<br>
			<div class="row">
				<input type="submit" value="Submit" class="btn btn-success">
			</div>
		{!! Form::close() !!}
	</div>
</div>


@endsection