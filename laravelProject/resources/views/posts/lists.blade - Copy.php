@extends("layout")

@section("content")
<h1>Post List</h1>
<div class="row">
<div class="col-md-6">
	<ul class="list-group">
		@foreach ($posts as $post) 
			<li class="list-group-item"> {{ $post->name }}</li>
		@endforeach
	</ul>
</div>
@endsection