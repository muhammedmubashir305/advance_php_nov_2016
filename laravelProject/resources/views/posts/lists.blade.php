@extends("layout")
@section("content")
	<h1>Post List</h1>
	<div class="row">
		<a class="btn btn-primary" href="{{ url('post/add') }}">Add Post</a>
	</div>
	<br>
	<div class="row">
		<div class="col-md-6">
			<ul class="list-group">
				@foreach($posts as $post)
					<a href="post/{{ $post->id }}/edit"><li class="list-group-item">{{ $post->name }}</li></a>
				@endforeach
			</ul>
		</div>
	</div> 	
@endsection