@extends("layout")
@section("content")

	<div class="col-md-12 col-md-offset-3">
		<h2>Add New Post</h2>
	</div>
	<form class="col-md-6 col-md-offset-3" method="post" action="{{ url('post/save') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="row">
			<input type="text" class="form-control" name="name">
		</div>
		<br>
		<div class="row">
			<textarea class="form-control" name="details"></textarea>
		</div>
		<br>
		<div class="row">
			<input type="submit" value="Submit" class="btn btn-success">
		</div>

	</form>
@stop