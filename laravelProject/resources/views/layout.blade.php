<html>
<head>
	<title>Laravel CMS</title>
	<link rel="stylesheet" type="text/css" href="{{ url ('/') }}/css/bootstrap.css">

</head>
<body>
	<div class="col-md-12 col-md-offset-3">
		<h1>Welcome to laravel CMS</h1>
	</div>
	<div id="container">
		@yield("content")
	</div>
</body>
</html>