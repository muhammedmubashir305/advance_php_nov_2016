<html>
<head>
	<title>Inner Laravel CMS</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	
</head>
<body>
	<h1>Welcome to laravel CMS - Inner Pages</h1>
	<div id="container">
		@yield("content")
	</div>
</body>
</html>