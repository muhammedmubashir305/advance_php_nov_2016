<?php
class validation
{
	public $errors = array();
	public function validate($data, $validation_rules)
	{
		$valid = TRUE;
		foreach ($validation_rules as $field => $rules) 
		{
				$callbacks = explode("|",$rules);
				// echo "<pre>";print_r($callbacks);
				foreach ($callbacks as $callback) 
				{
					$valid = $this->$callback($data[$field], $field);
				}
		}
		return $valid;
	}

	public function required($value,$fieldname)
	{
		if(empty($value))
		{
			$this->errors[] = "$fieldname cannot be blank";
			return false;
		}
		else
		{
			return true;
		}
	}

	public function email($value,$fieldname)
	{
		//regular expression 
		if(filter_var($value, FILTER_VALIDATE_EMAIL))
		{
			return true;
		}
		else
		{
			$this->errors[] = "$fieldname is not valid";
			return false;
		}
	}

	function length_checking($value,$fieldname, $min=0, $max=100)
	{
		$length = strlen($value);
		if($length >= $min && $length <= $max)
		{
			return true;
		}
		else
		{
			$this->errors[] = "$fieldname length is invalid";
			return false;
		}
	}
}
?>