<?php
// c:/windows/system32/drivers/etc
// hosts
//amount
//account
error_reporting(0);
session_start();
require "csrf.php";
$csrf_obj = new csrf();
$token = $csrf_obj->generate_token();

if(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === TRUE) 
{
	$amount = floatval($_GET['amount']);
	$account = intval($_GET['account']);
	
	if($amount > 0 && $account > 0)
	{
		$filename = "transaction_log.txt";
		$content = file_get_contents($filename);
		
		$msg = "Transfer of amount $amount has been made in $account \r\n";
		$content .= $msg;
		//verify
		if(file_put_contents($filename, $content))
		{
			echo $msg;
			//new generate
		}
		else
		{
			echo "transfer failed";
		}	
	}
	else
	{
		echo "Values missing";
	}
	
}
else
{
	echo "Please login first";
}
?>