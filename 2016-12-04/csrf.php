<?php
class csrf
{
	function __contruct()
	{
		session_start();
	}
	public function generate_token()
	{
		$token = hash("sha512",mt_rand(0,getrandmax().microtime()));
		
		$_SESSION['token'] = $token;
		return $token;
	}

	function check_token($token)
	{
		//check token verifcation from session
		$validate = (strlen($token) == 128 && $this->get_token_from_session() == $token);
		if($validate)
		{
			return TRUE;
		}
		else
			return FALSE;
	}

	function get_token_from_session()
	{
		return $_SESSION['token'];
	}

	function get_token_from_url()
	{
		$token = htmlentities($_GET['token']);
		return $token;
	}


}
?>