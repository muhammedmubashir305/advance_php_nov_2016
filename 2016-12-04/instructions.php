//laravel

//composer

5.0
5.1
5.2
5.3

Method#1
1. composer global require "laravel/installer"

2. laravel new projectName

method#2
composer create-project --prefer-dist laravel/laravel projectname "5.2.*"



<VirtualHost *:80>
    ServerName laravel.dev
    DocumentRoot "D:/xampp/htdocs/m2/laravelProject/public/"
    SetEnv APPLICATION_ENV "development"
    <Directory "D:/xampp/htdocs/m2/laravelProject/public/">
        DirectoryIndex index.php
        AllowOverride All
        Order allow,deny
        Allow from all
    </Directory>
</VirtualHost>